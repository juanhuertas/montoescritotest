package prueba.veritran.net.pruebauniversidad;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MontoEscrito {
	
	 private static Map<Integer,String> parseoNumeros = new HashMap<Integer,String>();
	
	public static void main (String [] arg) {
		
		Scanner sc =new Scanner(System.in);
		int n;
		System.out.print("Si Valor=");
		n=sc.nextInt();
		System.out.println("resultado= "+'"'+getMontoEscrito(n)+'"');
		
	}
	
	public static String getMontoEscrito(Integer valor){
		String resultado="",centenas="",miles="",millones="";
		int num, cant,und = 0,dec=0,cent=0,mil=0,decMil=0,centMil=0,millon=0;
		 crearParseNumeros();//intentar especificar mas en la cantidad para evitar ceros
		cant=String.valueOf(valor).length();
		
		
		if(valor>100000000||valor<0) {
			System.out.println("Error ,solo numeros en el rango de 0 a 100'000.000");
		}else if(cant==1) {//unidad
			
			return parseoNumeros.get(valor);
		
		}else if(cant==2) {//decena
			dec=valor;
			if((dec>=10&&dec<=30)||(dec%10==0)) {
				 return parseoNumeros.get(dec);
			}else {
				und=dec%10;
				dec-=und;
				centenas=parseoNumeros.get(dec);
				centenas=centenas+"y "+parseoNumeros.get(und);
				return resultado=centenas;
			}
			
		}if(cant==3) {//centena
			dec=valor%100;
			if((dec>=10&&dec<=30)||(dec%10==0)) {
				 centenas=parseoNumeros.get(dec);
			}else {
				und=dec%10;
				dec-=und;
				if(dec==0) {//si en las decenas hay un cero no lo imprime
				centenas=parseoNumeros.get(und);
				}else {
				centenas=parseoNumeros.get(dec)+" y "+parseoNumeros.get(und);
					
				}
			}
			cent=valor-(dec+und);
			if(cent%100==0) {
				if(cent==100&&und==0&&dec==0) {
					centenas=parseoNumeros.get(cent);
				}else if(cent==100&&und>0&&dec>0) {
					centenas=parseoNumeros.get(cent+1)+centenas;
				}else {
					centenas=parseoNumeros.get(cent)+centenas;
				}
			}
			
			
			return resultado=centenas;
		}else if(cant==4){//unidd de mil
			
			dec=valor%100;
			if((dec>=10&&dec<=30)||(dec%10==0)) {
				 centenas=parseoNumeros.get(dec);
			}else {
				und=dec%10;
				dec-=und;
				if(dec==0) {//si en las decenas hay un cero no lo imprime
				centenas=parseoNumeros.get(und);
				}else {
				centenas=parseoNumeros.get(dec)+" y "+parseoNumeros.get(und);
					
				}
			}
			cent=(valor-(dec+und))%1000;
			if(cent%100==0) {
				if(cent==100&&und==0&&dec==0) {
					centenas=parseoNumeros.get(cent);
				}else if(cent==100&&und>0&&dec>0) {
					centenas=parseoNumeros.get(cent+1)+centenas;
				}else if(cent==0){//si en las centenas  hay un cero no lo imprime
					centenas=""+centenas;
				}else {
					centenas=parseoNumeros.get(cent)+centenas;
				}
			}
			mil=(valor-(cent+dec+und));
			if(mil==1000&&cent==0&&dec==0&&und==0) {//si es 1000
				 resultado=parseoNumeros.get(mil);
			}else {
				mil=mil/1000;
				if(mil==1) {
					miles=parseoNumeros.get(1000);
					 resultado=miles+centenas;
				}else {
					miles=parseoNumeros.get(mil)+" "+parseoNumeros.get(1000);
					resultado=miles+centenas;
				}
				
			}
			return  resultado;
			
		}else if(cant==5){//decena de mil
			
			dec=valor%100;
			if((dec>=10&&dec<=30)||(dec%10==0)) {
				 centenas=parseoNumeros.get(dec);
			}else {
				und=dec%10;
				dec-=und;
				if(dec==0) {//si en las decenas hay un cero no lo imprime
				centenas=parseoNumeros.get(und);
				}else {
				centenas=parseoNumeros.get(dec)+" y "+parseoNumeros.get(und);
					
				}
			}
			cent=(valor-(dec+und))%1000;
			if(cent%100==0) {
				if(cent==100&&und==0&&dec==0) {
					centenas=parseoNumeros.get(cent);
				}else if(cent==100&&und>0&&dec>0) {
					centenas=parseoNumeros.get(cent+1)+centenas;
				}else if(cent==0){//si en las centenas  hay un cero no lo imprime
					centenas=""+centenas;
				}else {
					centenas=parseoNumeros.get(cent)+centenas;
				}
			}
			decMil=(valor-(cent+dec+und))/1000;
			
			if((decMil>=10&&decMil<=30)||(decMil%10==0)) {
				miles=parseoNumeros.get(decMil)+parseoNumeros.get(1000);
				
			}else {
				mil=decMil%10;
				
				decMil-=mil;
				if(mil==1) {
					miles=parseoNumeros.get(decMil)+" y un "+parseoNumeros.get(1000);
				}else {
				miles=parseoNumeros.get(decMil)+" y "+ parseoNumeros.get(mil)+parseoNumeros.get(1000);
				}
			}
				
			return resultado=miles+centenas;
		}else if(cant==6) {
			cent=valor%1000;
			mil=(valor-cent)/1000;
			// miles
			dec=valor%100;
			if((dec>=10&&dec<=30)||(dec%10==0)) {
				 centenas=parseoNumeros.get(dec);
			}else {
				und=dec%10;
				dec-=und;
				if(dec==0&&und>0) {//si en las decenas hay un cero no lo imprime
				centenas=parseoNumeros.get(und);
				}else if(dec==0&&und==0) {
				centenas="";
				}else{
				centenas=parseoNumeros.get(dec)+" y "+parseoNumeros.get(und);
					
				}
			}
			cent=(valor-(dec+und))%1000;
			if(cent%100==0) {
				if(cent==100&&und==0&&dec==0) {
					centenas=parseoNumeros.get(cent);
				}else if(cent==100&&und>0&&dec>0) {
					centenas=parseoNumeros.get(cent+1)+centenas;
				}else if(cent==0){//si en las centenas  hay un cero no lo imprime
					centenas=""+centenas;
				}else {
					centenas=parseoNumeros.get(cent)+centenas;
				}
			}
			decMil=((valor-(cent+dec+und))/1000)%100;
			
			if((decMil>=10&&decMil<=30)||(decMil%10==0)) {
				miles=parseoNumeros.get(decMil)+parseoNumeros.get(1000);
				
			}else {
				mil=decMil%10;
				
				decMil-=mil;
				
				if(decMil==0&&mil>0) {
					miles= parseoNumeros.get(mil)+parseoNumeros.get(1000);
				}else if(decMil==0&&mil==0){
					miles="";
				}else if(mil==1) {
					miles=parseoNumeros.get(decMil)+" y un "+parseoNumeros.get(1000);
				}else {
				miles=parseoNumeros.get(decMil)+" y "+ parseoNumeros.get(mil)+parseoNumeros.get(1000);
				}
			}
			
			centMil=(valor-((decMil*1000)+cent+dec+und))/100000;
		
			if(centMil==1&&decMil==0&&mil==0&&cent==0&&dec==0) {
				miles=parseoNumeros.get(100)+parseoNumeros.get(1000);
			}else if(centMil==1&&decMil==0) {
				miles=parseoNumeros.get(100)+parseoNumeros.get(1000);
			}else if(centMil==1&&(decMil>0||mil>0)){
				miles=parseoNumeros.get(101)+miles;
			}else {
				miles=parseoNumeros.get(centMil*100)+miles;
			}
			return resultado=miles +centenas;
		}else if (cant==7) {
			cent=valor%1000;
			mil=((valor-cent)/1000)%100;
			// miles
			
			dec=valor%100;
			
			if((dec>=10&&dec<=30)||(dec%10==0)) {
				 centenas=parseoNumeros.get(dec);
			}else {
				und=dec%10;
				dec-=und;
				if(dec==0) {//si en las decenas hay un cero no lo imprime
				centenas=parseoNumeros.get(und);
				}else {
				centenas=parseoNumeros.get(dec)+" y "+parseoNumeros.get(und);
					
				}
			}
			cent=(valor-(dec+und))%1000;
			if(cent%100==0) {
				if(cent==100&&und==0&&dec==0) {
					centenas=parseoNumeros.get(cent);
				}else if(cent==100&&und>0&&dec>0) {
					centenas=parseoNumeros.get(cent+1)+centenas;
				}else if(cent==0){//si en las centenas  hay un cero no lo imprime
					centenas=""+centenas;
				}else {
					centenas=parseoNumeros.get(cent)+centenas;
				}
			}
			decMil=((valor-(cent+dec+und))/1000)%100;
			mil=decMil%10;
			if((decMil>=10&&decMil<=30)||(decMil%10==0)) {
				miles=parseoNumeros.get(decMil)+parseoNumeros.get(1000);
				
			}else {
				
				decMil-=mil;
				
				if(decMil==0&&mil>0) {
					miles= parseoNumeros.get(mil)+parseoNumeros.get(1000);
				}else if(decMil==0&&mil==0){
					miles="";
				}else if(mil==1) {
					miles=parseoNumeros.get(decMil)+" y un "+parseoNumeros.get(1000);
				}else {
				miles=parseoNumeros.get(decMil)+" y "+ parseoNumeros.get(mil)+parseoNumeros.get(1000);
				}
			}
			
			centMil=((valor-((decMil*1000)+cent+dec+und))/100000)%10;/*

*/
			if(centMil==1&&decMil==0&&mil==0&&cent==0&&dec==0&&und==0) {
				miles=parseoNumeros.get(100)+parseoNumeros.get(1000);
			}else if(centMil==0) {
				miles=""+miles;
			}else if(centMil==1) {
				miles=parseoNumeros.get(101)+miles;
			}else {
				miles=parseoNumeros.get(centMil*100)+miles;
			}
			millon=(valor-((centMil*100000)+(decMil*1000)+(mil*1000)+cent+dec+und))/1000000;
			
			if(millon==1&&centMil==0&&decMil==0&&mil==0&&cent==0&&dec==0&&und==0) {
				millones=parseoNumeros.get(1000000);
			}else if(millon==1) {
				millones=parseoNumeros.get(1000000);
			}else {
				millones=parseoNumeros.get(millon)+parseoNumeros.get(1000001);
			}
			return resultado=millones+miles+centenas;
			
		}else if(cant==8) {
			cent=valor%1000;
			mil=((valor-cent)/1000)%1000;
			millon=(valor-(mil*1000+cent))/1000000;
			
					//millones
				num=millon;
				
			if(num==1) {
					millones=parseoNumeros.get(num*1000000);
			
			}else if(num<10&&num>1) {
				millones=parseoNumeros.get(num)+" millones ";
			}else if((num>=10&&num<=30)||(num%10==0)){
				millones=parseoNumeros.get(num)+ " millones ";
			}else {
				und=num%10;
				num=num-und;
				millones=parseoNumeros.get(num);
				millones=millones+"y "+parseoNumeros.get(und)+" millones ";
			}			//fin millones
			
						// miles
			num=mil%100;
						
			if((num>=10&&num<=30)||(num%10==0)) {
				 miles=parseoNumeros.get(num);
			}else {
					und=num%10;
					num=num-und;
					miles=parseoNumeros.get(num);
					miles=miles+"y "+parseoNumeros.get(und);
							
			}
			mil=mil-(num+und);
						
			if(mil%100==0) {
						
				miles=parseoNumeros.get(mil)+miles+" mil " ;
							
			}			//fin
						//centenas
			num=cent%100;
			if((num>=10&&num<=30)||(num%10==0)) {
				 centenas=parseoNumeros.get(num);
			}else {
					und=num%10;
					num-=und;
					centenas=parseoNumeros.get(num);
					centenas=centenas+"y "+parseoNumeros.get(und);
						 
			}
			num=cent-(num+und);
			if(num%100==0) {
						
				centenas=parseoNumeros.get(num)+centenas;
							
			}//fin centenas
					return resultado=millones+miles+centenas;	
		}else if(cant==9&&valor==100000000) {
			return resultado=parseoNumeros.get(valor);
		}
	return resultado;
	}

	
	  private static void crearParseNumeros() {
	        parseoNumeros.put(0,"cero ");
	        parseoNumeros.put(1,"uno ");
	        parseoNumeros.put(2,"dos ");
	        parseoNumeros.put(3,"tres ");
	        parseoNumeros.put(4,"cuatro ");
	        parseoNumeros.put(5,"cinco ");
	        parseoNumeros.put(6,"seis ");
	        parseoNumeros.put(7,"siete ");
	        parseoNumeros.put(8,"ocho ");
	        parseoNumeros.put(9,"nueve ");
	        parseoNumeros.put(10, "diez ");
	        parseoNumeros.put(11, "once ");
	        parseoNumeros.put(12, "doce ");
	        parseoNumeros.put(13, "trece ");
	        parseoNumeros.put(14, "catorce ");
	        parseoNumeros.put(15, "quince ");
	        parseoNumeros.put(16,"dieciseis ");
	        parseoNumeros.put(17,"diecisiete ");
	        parseoNumeros.put(18,"dieciocho ");
	        parseoNumeros.put(19,"diecinueve ");
	        parseoNumeros.put(20, "veinte ");
	        parseoNumeros.put(21,"veintiuno ");
	        parseoNumeros.put(22,"veintidos ");
	        parseoNumeros.put(23,"veintitres ");
	        parseoNumeros.put(24,"veinticuatro ");
	        parseoNumeros.put(25,"veinticinco ");
	        parseoNumeros.put(26,"veintiseis ");
	        parseoNumeros.put(27,"veintisiete ");
	        parseoNumeros.put(28,"veintiocho ");
	        parseoNumeros.put(29,"veintinueve ");
	        parseoNumeros.put(30, "treinta ");
	        parseoNumeros.put(40, "cuarenta ");
	        parseoNumeros.put(50, "cincuenta ");
	        parseoNumeros.put(60, "sesenta ");
	        parseoNumeros.put(70, "setenta ");
	        parseoNumeros.put(80, "ochenta ");
	        parseoNumeros.put(90, "noventa ");
	        parseoNumeros.put(100, "cien ");
	        parseoNumeros.put(101,"ciento ");
	        parseoNumeros.put(200,"doscientos ");
	        parseoNumeros.put(300,"trescientos ");
	        parseoNumeros.put(400,"cuatrocientos ");
	        parseoNumeros.put(500, "quinientos ");
	        parseoNumeros.put(600,"seiscientos ");
	        parseoNumeros.put(700,"setecientos ");
	        parseoNumeros.put(800,"ochocientos ");
	        parseoNumeros.put(900,"novecientos ");
	        parseoNumeros.put(1000, "mil ");
	        parseoNumeros.put(1000000, "un millon ");
	        parseoNumeros.put(1000001, " millones ");
	        parseoNumeros.put(100000000,"cien millones");
	        
	        
	    }
}
